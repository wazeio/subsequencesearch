'use strict';

// TODO фильтр идентичных подпоследовательностей
// TODO Дробление цепочек на составляющие процессы
var processParser = (function(){
    function createMap(eventList) {
        var map = [];
        // Создаем карту совпадений в двумерном массиве
        console.log(JSON.stringify(eventList));
        for (let y=0; y<eventList.length; y++) {
            let reference = eventList[y];
            let counter = 0;
            map[y] = [];
            for (let x=y+1; x<eventList.length; x++) {
                let value = eventList[x];
                // Если значения равны
                if (reference === value) {
                    // Высчитываем коэффициент встречаемости
                    // Нужен, чтобы определять как часто встречаются элементы
                    map[y][x] = {
                        count: ++counter,
                        value: value,
                    };
                    // console.log('Map item on [' + y + ', ' + x + ']: ' + value + ', occurrence: ' + itemOccurrence[value] + ', itemFreq: ' + itemOccurrence[value]);
                } else {
                    map[y][x] = {};
                }
            }
        }
        return map;
    }
    function getOccurrenceForSubSequence(map, x, y, reference) {
        if (y >= x) {
            return 1;
        }
        let linePointList = map[y];
        for (let i = x; i<linePointList.length; i++) {
            let point = linePointList[i];
            let value = point.value;
            let count = point.count;
            if (value) {
                if (reference === count) {
                    return count + 1;
                }
                return count;
            }
        }
        return 0;
    }
    function getSubSequencesFromPoint(parameters) {
        var map = parameters.map;
        var y = parameters.y || 0;
        var x = parameters.x || 1;
        var subSequences = [];
        var sequenceLength = map.length;
        var minOccurrence = map[y][x].count;
        var maxOccurrence = minOccurrence;
        var sequence = [map[y][x].value];
        var aggregatedIndexList = [x];
        x++;
        y++;
        while(1) {
            // Если оказались в пустой зоне карты
            if (x <= y) {
                x = y + 1;
            }
            // Если вышли за пределы карты
            if (x >= sequenceLength) {
                // x = предыдущий x
                x = aggregatedIndexList[aggregatedIndexList.length - 1] + 1;
                y++;
                // Если карта закончилась
                if (y >= sequenceLength) break;
                // Повторить проверку
                continue;
            }
            // Если карта закончилась
            if (y >= sequenceLength) break;
            // Если есть совпадение на карте
            let mapItem = map[y][x];
            if (mapItem.value) {
                // Если текущего y,х нет среди предыдущих x
                if (aggregatedIndexList.indexOf(y) === -1 && aggregatedIndexList.indexOf(x) === -1) {
                    minOccurrence = mapItem.count > minOccurrence ? minOccurrence : mapItem.count;
                    maxOccurrence = mapItem.count < maxOccurrence ? maxOccurrence : mapItem.count;
                    // Дополняем цепочку данными
                    sequence.push(mapItem.value);
                    // Оформляем вариант цепочки
                    let subSequence = sequence.slice(0);
                    let occurrence = getOccurrenceForSubSequence(map, parameters.x, y, minOccurrence);
                    // Фильтрация неповторяемых цепочек
                    if (occurrence > 1) {
                        subSequence.push(occurrence);
                        // Добавляем цепочку
                        subSequences.push(subSequence);
                        // Фиксируем точку совпадения
                        aggregatedIndexList.push(x++);
                    }
                    y++;
                } else {
                    y++;
                }
                // Если на карте нет этой точки
            } else {
                x++;
            }
        }
        return subSequences;
    }
    return {
        createMap: createMap,
        searchSubSequences(map) {
            var subSequences = [];
            for (let y=0; y<map.length; y++) {
                for (let x=y+1; x<map.length; x++) {
                    // Если есть совпадение в этой точке
                    if (map[y][x].value) {
                        subSequences = subSequences.concat(getSubSequencesFromPoint({
                            map: map,
                            x: x,
                            y: y
                        }));
                        console.log('[' + y + ', ' + x + '] done');
                    }
                }
            }
            subSequences.sort((a, b) => {
                var length1 = a.length;
                var length2 = b.length;
                var occurrence1 = a[length1 - 1];
                var occurrence2 = b[length2 - 1];
                if (length1 === length2) {
                    return occurrence2 - occurrence1;
                } else {
                    return length2 - length1;
                }
            });
            console.log('Длина исходного массива: ' + map.length);
            console.log('Количество найденных цепочек: ' + subSequences.length);
            return subSequences;
        },
        parse: function(list) {
            var map = createMap(list);
            console.log('Map creation done ' + map.length);
            return this.searchSubSequences(map);
        }
    }
})();

// var process = ['A','A','A','A','A','A','C','D','A','C','D','B','D','D','A','C','D','B','D','D','A','C','D','B','D','D','A','C','D','B','D','D'];
// var process = ['A','B','C','D','E','F','G','A','B','C','D','E','F','D','E','F','G','A','B','C','D','E','F','D'];
// var process = 'П1aрsиd2fПgрh3hjивеk5lт;в6ет 7xВсеv Всn8емz9м';
// var process = ['q','w','e','r','t','y','y','q','w','e',
//     'r','t','y','q','w','e','r','t','t','y',
//     'q','w','e','r','t','y','q','q','w','e',
//     'r','t','y','q','w','e','r','r','t','y'];
// var process = 'qwer1t2yqw3ert4yq5wer6ty7qw8ert9yq0wer3tyqw5erty3';
// var process = 'qwe1rt2yqw3ert4yqw5erqwety6rty';
// var process = 'ПриПривет вкак ет дела как дела? Ну как же дела?';


// var process = 'ТворожокТворожок? Чё? Пошехонский? Пошехонский!';
var process = '123.123.123.123.12123.3.123.123.123.123.123.123.123.123.123.' +
    '123.123.123.123.12123.3.123.123.123.123.123.123.123.123.123.' +
    '123.123.123.123.12123.3.123.123.123.123.123.123.123.123.123.' +
    '123.123.123.123.12123.3.123.123.123.123.123.123.123.123.123.' +
    '123.123.123.123.12123.3.123.123.123.123.123.123.123.123.123.' +
    '123.123.123.123.12123.3.123.123.123.123.123.123.123.123.123.' +
    '123.123.123.123.12123.3.123.123.123.123.123.123.123.123.123.' +
    '123.123.123.123.12123.3.123.123.123.123.123.123.123.123.123.' +
    '123.123.123.123.12123.3.123.123.123.123.123.123.123.123.123.' +
    '123.123.123.123.12123.3.123.123.123.123.123.123.123.123.123.';
// var process = 'ejfhwufhwiqwoiutguiernfdsnvbnzxcbhekvmntyomernbwsyfiwtghsg';
console.time('Time');
var bps = processParser.parse(process);
var longestSub = [0];
var mostFreqSub = [0];
console.log(JSON.stringify(bps));

for (let i=0; i<bps.length; i++) {
    let bp = bps[i];
    let freq = bp[bp.length - 1];
    let length = bp.length;
    if (length > 2 && freq > mostFreqSub[mostFreqSub.length - 1]) {
        mostFreqSub = bp;
    }
    if (length > longestSub.length) {
        longestSub = bp;
    }
}
console.timeEnd('Time');
var count1 = longestSub[longestSub.length - 1];
var count2 = mostFreqSub[mostFreqSub.length - 1];
// console.log(bps);
longestSub.splice(-1, 1);
let recoverProcess = '';
let stringLongestSub = longestSub.join('');
for (let i=0; i<count1; i++) {
    recoverProcess = recoverProcess + stringLongestSub;
}
console.log('Исходная последовательность: ' + process);
console.log('Исправленная последовательность: ' + recoverProcess);
console.log('Самая длинная последовательность: ' + longestSub.join('') + ', повторяется ' + count1 + ' раз');
console.log('Наиболее часто встречаемая: ' + JSON.stringify(mostFreqSub) + ', повторяется ' + count2 + ' раз');